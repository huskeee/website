{
    "api": "0.13",
    "space": "Hackerspace.gr",
    "logo": "https://www.hackerspace.gr/images/hsgr.png",
    "url": "https://www.hackerspace.gr",
    "location": {
        "address": "Irous 21, 10442 Athens, Greece",
        "lon": 23.70649,
        "lat": 37.99908
    },
    "state": {
        "open": ${OPEN},
        "lastchange": ${TIMESTAMP},
	"message": "${MESSAGE}"
    },
    "contact": {
        "irc": "irc://chat.freenode.net/#hsgr",
        "matrix" "https://riot.im/app/#/room/#hsgr:matrix.org",
        "phone": "+302130210437",
        "facebook": "hackerspacegr",
        "twitter": "@hackerspacegr",
        "ml": "discuss@hackerspace.gr",
        "issue_mail": "infra@hackerspace.gr"
    },
    "issue_report_channels": [
        "issue_mail"
    ],
    "feeds": {
        "updates": {
            "type": "application/rss+atom",
            "url": "https://www.hackerspace.gr/feed/"
        },
        "calendar": {
            "type": "text/calendar",
            "url": "https://www.hackerspace.gr/archive/hsgr.ics"
        }
    },
    "projects": [
        "http://github.com/hsgr",
        "https://www.hackerspace.gr/wiki/Category:Projects"
    ],
    "cache": {
        "schedule": "m.02"
    }
}

