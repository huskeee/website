+++
title = "Frequently Asked Questions"
slug = "faq"
thumbnail = "images/tn.png"
description = "faq"
+++



#### How often is hackerspace open?

In theory, the space is open 24/7. But if you want to be 100% sure you can check the events tab. If there is an event happening, then the space is definitely open. You can also check the counter on the main page of our website, which is auto-updated with the current number of connected devices to the hackerspace network. And of course you can always call, old school style !

#### If I don't know an HSGR member or anyone from the community, can I still come?

Of course! The space is open for everyone. If it's your first time at the spae you can introduce yourself upon arrival to get a short tour from an available meber. And don't forget to read our [vision](../en/about) !

#### If I'm not a hacker can I come?

Hacking is about sharing and being innately curious. Don't get intimidated by the term. If you are interested in sharing your knowledge or learn from others, then hackerspace is definitely the place to be ! Our inspiration comes from the Open Source philosophy.

#### Is hackerspace a co-working space?

It's a space that fosters collaboration between people that share its [vision](../en/about), but doesn't offer the possibility of leasing spaces and infrastructure under any case. The space is always open for projects, presentations and meetings that abide to open source practicies.

#### What should I do to organize an event at the hackerspace?

Hackerspace is open to host any event which aligns with our [vision](../en/about). The important thing is to find one of the members, who can open the space for the event. This can happen in person at the hackerspace, or through our [Matrix HSGR chat](../en/contact). Also, if you want to discuss and prepare your event, send an email to our [mailing list](../en/contact) explaining it in a concise manner. The available mebers can further help you create an event entry to the website/wiki and guide you through any logistics.

#### What does the space offer? What should I bring with me?

Besides getting to know and interact with interesting people and a vibrant community, the space offers free internet access, books and magazines you can read, workbenches and a great variety of tools and machinery. What you should bring is the will to communicate, share and optionally your laptop. You will, also, find the necessary infrastructure to get your caffeine fix and a fridge full of beers and beverages !

#### Can I use any equipment/tools/heavy machinery?

The sole prerequisite is the project at hand to be open source. The space hosts a variety of heavy machinery and tools, if you're not that acquainted with what you need to use for your project, ask an available member to show you how to properly use them. Mainly, for your own safety ! We keep an [inventory](https://www.hackerspace.gr/wiki/Inventory) of the hackerspace items on our wiki page and the how-to-use [documentation](https://www.hackerspace.gr/wiki/Category:Documentation). Feel free to contribute ! 

#### Can I use the hackerespace consumables?

Yes of curse, provided you renew them in some way.

#### Can I use the space for my Start-up or to use the equipment for commercial use?

Yes, as long as they are open source or open hardware. Production line could be accommodated as long as it doesn’t disrupt the space operation.

#### Can I use the equipment to repair something for which I will receive payment?

Yes, as long as you make the procedure public and communicate the knowledge.

#### IMPORTANT: Who cleans up the space?

Everybody, including you. You are free to use anything available from the kitchen (coffee machines, glasses, dishes, etc). But remember, you should clean up after and possibly leave the space cleaner than you found it.
